import numpy as np
import sys
from numpy.linalg import linalg


n = 3
epsilon = 10**(-10)
kMax = 10000

# A = [[1,2],[3,4]]
A = [[1,2,0],
     [0,1,2],
     [0,0,1]]

def GetInitialVMatrix(A):
    At = np.matrix(A).transpose()
    A1 =GetA1(A)
    AInf = GetAInfinit(A)
    div = A1 * AInf
    # for i in range(n):
    #     for j in range(n):
    #         At[i][j]/=div
    return np.matrix(At)/div

def GetA1(A):
    max = 0
    for j in range(n):
        sum = 0
        for i in range(n):
            sum+=abs(A[i][j])
        if(sum>max):
            max = sum
    return max

def GetAInfinit(A):
    max = 0
    for i in range(n):
        sum = 0
        for j in range(n):
            sum+=abs(A[i][j])
        if(sum>max):
            max = sum
    return max


def CheckInvertibleMatrix(A):
    det = np.linalg.det(A)
    if det == 0 and linalg.cond(A) < 1/sys.float_info.epsilon :
        return False
    return True

# V0 = GetInitialVMatrix(A)

def SchultzMethod(Vk,A):
    Vk = np.matrix(Vk)
    A = np.matrix(A)
    Vk1 =  np.matmul(Vk,((2* np.identity(n))-(np.matmul(    A,Vk))))
    return Vk1

def LiMethod1(Vk,A):
    Vk = np.matrix(Vk)
    A = np.matrix(A)
    Vk1 =  np.matmul(Vk,3* np.identity(n)-np.matmul(np.matmul(A,Vk),3* np.identity(n)-np.matmul(A,Vk)))
    return Vk1

def LiMethod2(Vk,A):
    Vk = np.matrix(Vk)
    A = np.matrix(A)
    Vk1 =  np.matmul(np.identity(n)+0.25*np.matmul(np.identity(n)-np.matmul(Vk,A),np.matmul(3*np.identity(n)-np.matmul(Vk,A),3*np.identity(n)-np.matmul(Vk,A))),Vk)
    return Vk1

def IterativMethod(A,method):
    if(CheckInvertibleMatrix(A) is not True ):
        print('Matricea nu este singulara')
        return
    V0 = V1 = np.matrix(GetInitialVMatrix(A))
    k=0
    global epsilon
    global kMax
    deltaV = 1
    tenPow = 10**10
    while(deltaV>=epsilon and k<=kMax and deltaV <= tenPow):
        V0 = V1

        # print('iteratia')
        # print(k)
        if(method == 1):
            V1 = SchultzMethod(V0,A)
        if (method == 2):
            V1 = LiMethod1(V0, A)
        if (method == 3):
            V1 = LiMethod2(V0, A)
        # print('---',k)
        # # print(np.matrix(V0))
        # # print(np.matrix(V1))
        # # print(np.subtract(np.matrix(V1),np.matrix(V0)))
        # print(np.linalg.det(np.subtract(np.matrix(V1),np.matrix(V0))))
        # print('---')

        deltaV = np.linalg.norm(np.subtract(np.matrix(V1),np.matrix(V0)),1)
        k+=1
    if(deltaV<epsilon):
        # print(np.matmul(A,V1),k)
        print(" -------- Rezultat",method)
        print(np.linalg.det(np.matmul(A,V1)-np.identity(n))) # norma
    else:
        print('Divergent')
    return V1


def InductivDeduction(maximIterations):
    for iter in range(maximIterations):
        matrix =[[0 for x in range(iter)] for y in range(iter)]
        if(iter > 1):
            for i in range(iter):
                for j in range(iter):
                    if(i == j):
                        matrix[i][j] = 1
                    else:
                        if(i+1 == j):
                            matrix[i][j] = 2
            # print(matrix)
            global n
            n= iter
            print(np.matrix(IterativMethod(matrix,1)))

# print(SchultzMethod(V0,A))
# print(LiMethod1(V0,A))
# print(LiMethod2(V0,A))

IterativMethod(A,1)
IterativMethod(A,2)
IterativMethod(A,3)
# InductivDeduction(8)
